package edu.epam.izhevsk.junit;

import static org.junit.Assert.*;
import static org.mockito.Mockito.atLeastOnce;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TestRule;
import org.junit.rules.Timeout;
import org.junit.runner.RunWith;
import org.mockito.AdditionalMatchers;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;


@RunWith(MockitoJUnitRunner.class)
public class PaymentControllerTest {
  @Rule
  public TestRule timeout = new Timeout(100);

  @Rule
  public ExpectedException thrown = ExpectedException.none();

  @Mock
  private AccountService account;
  @Mock
  private DepositService deposit;
  @InjectMocks
  private PaymentController controller;

  @SuppressWarnings("unchecked")
  @Before
  public void setUp() throws InsufficientFundsException {
    when(account.isUserAuthenticated(100l)).thenReturn(true);
    when(deposit.deposit(AdditionalMatchers.lt(100l), AdditionalMatchers.leq(100l)))
        .thenReturn("Успех");
    when(deposit.deposit(AdditionalMatchers.gt(100l), Matchers.anyLong()))
        .thenThrow(InsufficientFundsException.class).thenThrow(InsufficientFundsException.class);
  }

  @Test
  public void testDepositSuccessful() throws InsufficientFundsException {
    controller.deposit(100l, 100l);
    verify(deposit, atLeastOnce()).deposit(100l, 100l);
  }

  @Test
  public void testDepositAmountLt100() throws InsufficientFundsException {
    assertEquals("Успех", deposit.deposit(20l, 3l));
  }


  @Test
  public void testFailAmountGt100() {
    try {
      deposit.deposit(1034l, 90l);
    } catch (InsufficientFundsException e) {
      // TODO Auto-generated catch block
      System.out.println(e.getMessage());
    }


  }

  @Test
  public void testFaildUnauthenticated() {
    try {
      controller.deposit(10l, 39870l);
    } catch (InsufficientFundsException | SecurityException e) {
      // TODO Auto-generated catch block
      System.out.println(e.getMessage());
    }


  }

}

